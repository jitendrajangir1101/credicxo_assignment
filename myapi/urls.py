from django.urls import path,include
from .views import studentRegistrationView ,teacherRegistrationView
from . import views
from .views import MyObtainTokenPairView ,ChangePasswordView ,AllStudentsGetAPI,StudentRegisterAPI
from rest_framework_simplejwt.views import TokenRefreshView



urlpatterns = [
    path('api/registration/student/', studentRegistrationView.as_view(), name='register-student'),
    path('api/registration/teacher/', teacherRegistrationView.as_view(), name='register-teacher'),
    path('api/login/', MyObtainTokenPairView.as_view(), name='token_obtain_pair'),
    path('login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/password_reset/',
         include('django_rest_passwordreset.urls', namespace='password_reset')),
    path('api/change-password/', ChangePasswordView.as_view(),
         name='change-password'),
    path('api/studentsList/',views.AllStudentsGetAPI , name='studentlist'),
    path('api/studentsCreate/',views.StudentRegisterAPI , name='studentcreate'),
    path('api/studentinfo/<int:pk>',views.StudentinfoAPI , name='studentinfo'),
    
    
]
