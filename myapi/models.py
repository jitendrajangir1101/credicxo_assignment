from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser
from django.conf import settings

#Custom user model
class Profile(AbstractUser):
  #Boolean fields to select the type of account.
  is_student = models.BooleanField(default=False)
  is_teacher = models.BooleanField(default=False)
#Student model 
class student(models.Model):
    student = models.OneToOneField(
      Profile, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    

    def __str__(self):
        return self.student.username
#Teacher Model
class teacher(models.Model):
    teacher = models.OneToOneField(
        Profile, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    subjectName = models.CharField(max_length=100)

    def __str__(self):
        return self.teacher.username