from django.shortcuts import render
from rest_auth.registration.views import RegisterView
from .models import Profile ,student ,teacher
from rest_framework.decorators import api_view ,permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from django.shortcuts import render,get_object_or_404 ,redirect
from rest_framework import generics, permissions
from django.contrib.auth import authenticate, login, logout
from rest_framework import status
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.decorators import permission_required
# Create your views here.
from .serializers import (
    studentCustomRegistrationSerializer, teacherCustomRegistrationSerializer ,UserSerializer ,ChangePasswordSerializer,studentSerializer
    )

from .serializers import MyTokenObtainPairSerializer
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.views import TokenObtainPairView


# I am using this class for obtaining token.
class MyObtainTokenPairView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = MyTokenObtainPairSerializer
# I am using this class for students registration
class studentRegistrationView(RegisterView):
    serializer_class = studentCustomRegistrationSerializer

# I am using this class for Teacher registration
class teacherRegistrationView(RegisterView):
    serializer_class = teacherCustomRegistrationSerializer

# This class is for change password
class ChangePasswordView(generics.UpdateAPIView):
    """
    An endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    model = Profile
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# this function is used to get all the students data.
@api_view(['GET'])
def AllStudentsGetAPI(request):
    group = request.user.groups.values_list('name', flat=True)
    if group == "Teacher":

        try:
            student = student.objects.all()
            student_serializer = studentSerializer(student, many=True)
            return JsonResponse(student_serializer.data, safe=False)
        

        except student.DoesNotExist:
        
            return Response({
                "msg": "students not found",
            })
    else:
        return Response({
                "msg": "student can not visit this ",
            })
#this function is used for adding student information by a teacher
@api_view(['POST'])
def StudentRegisterAPI(request):
    group = request.user.groups.values_list('name', flat=True)
    if group == "Teacher":

        try:
            student_data = JSONParser().parse(request)
            user = studentSerializer(student, data=student_data, partial=True)
            group = Group.objects.get(name = 'Student')
            user.groups.add(group)
            return Response({
            "msg": " student data saved ",
            })

        except :
        
            return Response({
                "msg": "students not added",
            })
    else:
        return Response({
                "msg": "student can not added try again ",
            })



# this function is used for getting a particular student info based on his primary key field.
@api_view(['GET'])
def StudentinfoAPI(request, pk):
    try:
        member = student.objects.get(pk=pk)
        student_serializer =studentSerializer(member)
        return JsonResponse(student_serializer.data)
        
        # return JsonResponse({'message': 'member was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)

    except student.DoesNotExist:
        # return JsonResponse({'message': 'The tutorial does not exist'}, status=status.HTTP_404_NOT_FOUND)
        return Response({
            "msg": "your profile not found",
        })

    