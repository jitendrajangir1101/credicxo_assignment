from rest_framework import serializers
from rest_auth.registration.serializers import RegisterSerializer
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import Group

from .models import student ,teacher ,Profile
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

# Serializer for getting student data also registering student data by a teacher.
class studentSerializer(serializers.ModelSerializer):

    class Meta:
        model = student
        fields = ('id', 'choice_text')

#Serializer for changing password.
class ChangePasswordSerializer(serializers.Serializer):
    model = Profile

    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

#Serializer for obtaining Token.
class MyTokenObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super(MyTokenObtainPairSerializer, cls).get_token(user)

        # Add custom claims
        token['username'] = user.username
        return token
##Serializer for user operations.
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('username','password')

#Serializer for registering a student and also assigning to a particular group called student.
class studentCustomRegistrationSerializer(RegisterSerializer):
    student = serializers.PrimaryKeyRelatedField(read_only=True,) #by default allow_null = False
    name = serializers.CharField(required=True)
    address = serializers.CharField(required=True)
    
    
    def get_cleaned_data(self):
            data = super(studentCustomRegistrationSerializer, self).get_cleaned_data()
            extra_data = {
                'name' : self.validated_data.get('name', ''),
                'address' : self.validated_data.get('address', ''),
                
            }
            data.update(extra_data)
            return data

    def save(self, request):
        user = super(studentCustomRegistrationSerializer, self).save(request)
        user.is_student = True
        group = Group.objects.get(name = 'Student')
        user.groups.add(group)
        user.save()
        st = student(student=user, name=self.cleaned_data.get('name'), address=self.cleaned_data.get('address'))
                
        st.save()
        return user

#Serializer for registering a teacher and also assigning to a particular group called teacher.
class teacherCustomRegistrationSerializer(RegisterSerializer):
    teacher = serializers.PrimaryKeyRelatedField(read_only=True,) #by default allow_null = False
    name = serializers.CharField(required=True)
    subjectName = serializers.CharField(required=True)
    
    def get_cleaned_data(self):
            data = super(teacherCustomRegistrationSerializer, self).get_cleaned_data()
            extra_data = {
                'name' : self.validated_data.get('name', ''),
                'subjectName' : self.validated_data.get('subjectName', ''),
            }
            data.update(extra_data)
            return data

    def save(self, request):
        user = super(teacherCustomRegistrationSerializer, self).save(request)
        user.is_teacher = True
        group = Group.objects.get(name = 'Teacher')
        user.groups.add(group)
        user.save()
        t = teacher(teacher=user, name=self.cleaned_data.get('name'), subjectName=self.cleaned_data.get('subjectName'))
        t.save()
        return user